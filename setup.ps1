﻿$CurrentPath = Split-Path -LiteralPath $PSCommandPath
Write-Output "Running relative to $CurrentPath"


$IIS = @{
    SiteName = "127.0.0.1"
    AppPoolName = "SSLCertAuthTestPool"
    Path = (Join-Path -Path $CurrentPath -ChildPath "MutualSSLAuth" -Resolve )
    Bindings = @(
        @{ protocol = "https"; bindingInformation = "127.0.0.1:9443:" }
    )
}

$CACertificate = @{
    File = (Join-Path $CurrentPath "dummyca\certs\ca.crt")
    Store = "Cert:\LocalMachine\Root"
}

$Certificate = @{
    File = (Join-Path $CurrentPath "dummyca\server.pfx")
    Password = "server"
    Store = "Cert:\LocalMachine\My"
    Thumbprint = "9913DB892A95093CE22772B010897EA3B4A0D54B"
}

try
{
    Write-Output "Attempting to import the WebAdministration module"
    Import-Module WebAdministration -ErrorAction Stop
}
catch
{
    Write-Error $_
    Write-Output "Well, that didn't work. Sorry."
    Exit 1
}

Write-Output "Importing CA certificate from $($CACertificate.File)"
Import-Certificate -FilePath $CACertificate.File -CertStoreLocation $CACertificate.Store -ErrorAction Stop

Write-Output "Importing server certificate from $($CACertificate.File)"
Import-PfxCertificate -FilePath $Certificate.File -Password (ConvertTo-SecureString -String $Certificate.Password -Force -AsPlainText) -CertStoreLocation $Certificate.Store -ErrorAction Stop


Write-Output "Creating App Pool: $($IIS.AppPoolName)"
if ( Test-Path IIS:\AppPools\$($IIS.AppPoolName) )
{
    Write-Output "App Pool already exists! Should remove it first.."
    Remove-Item IIS:\AppPools\$($IIS.AppPoolName)
}
$AppPool = New-Item IIS:\AppPools\$($IIS.AppPoolName) -Force
$AppPool.managedRuntimeVersion = "v4.0"
$AppPool | Set-Item

Write-Output "Creating Website: $($IIS.SiteName)"
if ( Test-Path IIS:\Sites\$($IIS.SiteName) )
{
    Write-Output "Website already exists! Should remove it first.."
    Remove-Item IIS:\Sites\$($IIS.SiteName)
}
New-Item IIS:\Sites\$($IIS.SiteName) -PhysicalPath $IIS.Path -Bindings $IIS.Bindings -applicationPool $IIS.AppPoolName

Write-Output "Binding SSL Certificate"
if ( Test-Path IIS:\SslBindings\127.0.0.1!9443 )
{
    Remove-Item IIS:\SslBindings\127.0.0.1!9443
}
$Cert = Get-Item (Join-Path $Certificate.Store $Certificate.Thumbprint)
Push-Location
Set-Location IIS:\SslBindings
$Cert | New-Item 127.0.0.1!9443 -ErrorAction Stop | Out-Null
Pop-Location

Write-Output "Configure SSL Client Auth"
Set-WebConfigurationProperty -PSPath IIS:\Sites\$($IIS.SiteName) -Filter system.webServer/security/access -Name SslFlags -Value "Ssl,SslNegotiateCert"

& "$env:windir\System32\inetsrv\appcmd.exe" unlock config /section:system.webServer/security/access

Write-Output "Updating SChannel ClientAuthTrustMode to '2' in Registry so IIS trust client certs easily"
Write-Output "   see (https://technet.microsoft.com/en-gb/library/dn786429.aspx)"
Set-ItemProperty -Path HKLM:\System\CurrentControlSet\Control\SecurityProviders\SChannel -Name ClientAuthTrustMode -Value 2