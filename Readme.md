setup.ps1 to configure your local env (setup IIS, install certs, tweak registry). Run it from the root dir of the solution as it uses relative paths, and run with elevated privs.

It assumes you have full IIS installed locally and will probably fail spectacularly if you don�t.

I�ve used two Owin middleware packages from Thinktecture to do the Cert -> Authenticated User mapping, and you can find the source here if you want to see what they do: https://github.com/IdentityModel/Thinktecture.IdentityModel/tree/master/source

ConsoleApplication1  is a helpfully named client app..

After setup and build, site lives at https://127.0.0.1:9443/api/identity

There are two certs to use with the client app, client1-a.pfx and client2-a.pfx [1]

I�ve created my own CA for testing, using OpenSSL, the contents of which are in the dummyca folder. The hierarchy of certs being

CA
|- server (used to secure the https endpoint in IIS)
|- client1-a (useable for client-auth)
|- client2-a (useable for client-auth)

CA gets installed into Local Machine scope Trusted Root Certification Authorities store, server (with private key) gets installed into Local Machine scope Personal store to be used by IIS binding. Client certs stay as .pfx (aka PKCS#12) on disk, protected by the passphrase �a�. You can import them into Current User scope Personal store if you want to hit the service from $Browser � you�ll probably want to put the CA cert into your user�s trusted root store also.

The test harness can use either client1-a or client2-a to authenticate to the service. In fact if you have any valid (in-date, with client-auth extensions) trusted (signed by CA in trusted roots machine store) cert available you�ll be able to authenticate to this service. Therefore, the exercise of writing some Owin middleware or an AuthorizeAttributeFilter that restricts access based on issuer, subject and/or thumbprint is left as an exercise for the reader�

It is possible to restrict IIS to only trusting (for client-auth) certs signed by a specific issuer list. Details are here, under �How It Works�, specifically �Configuring an application or feature to use the Client Authentication Issuers store�, but I haven�t tested it.

https://technet.microsoft.com/en-gb/library/dn786429.aspx

But that only locks you down to issuer, not to specific cert(s).

Hope it makes sense (and works..)

There are doubtless other ways  - some of them might even be easier.. 
