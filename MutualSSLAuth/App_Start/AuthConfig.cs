﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Thinktecture.IdentityModel.Owin;

namespace MutualSSLAuth
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.RequireSsl(requireClientCertificate: true);

            app.UseClientCertificateAuthentication(System.Security.Cryptography.X509Certificates.X509RevocationMode.NoCheck);
        }
    }
}