﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MutualSSLAuth.Models
{
    public class Identity
    {
        public string Issuer { get; set; }
        public string Subject { get; set; }
        public string Thumbprint { get; set; }
    }
}