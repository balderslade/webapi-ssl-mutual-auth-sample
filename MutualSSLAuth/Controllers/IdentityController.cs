﻿using MutualSSLAuth.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MutualSSLAuth.Controllers
{
    public class IdentityController : ApiController
    {
        [Authorize]
        public Identity Get()
        {
            var User = Request.GetOwinContext().Authentication.User;

            var identity = new Identity
            {
                Issuer = User.Claims.FirstOrDefault(c => c.Type == "issuer").Value,
                Subject = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.X500DistinguishedName).Value,
                Thumbprint = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Thumbprint).Value
            };

            return identity;
        }
    }
}
