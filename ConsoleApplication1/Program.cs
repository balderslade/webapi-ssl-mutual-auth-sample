﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Configuration;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // You'll need this if your /server/ cert is not trusted.. or you can use this type of mechanism for certificate pinning.
            //ServicePointManager.ServerCertificateValidationCallback +=
            //        (sender, cer, chain, sslPolicyErrors) => true;

            var path = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            var certFile = Path.Combine(path, ConfigurationManager.AppSettings["Certificate.FileName"]);

            var cert = new X509Certificate2();
            cert.Import(
                fileName: certFile,
                password: ConfigurationManager.AppSettings["Certificate.Password"],
                keyStorageFlags: X509KeyStorageFlags.UserKeySet
            );

            var handler = new WebRequestHandler();
            handler.ClientCertificates.Add(cert);

            var client = new HttpClient(handler)
            {
                BaseAddress = new Uri("https://127.0.0.1:9443/")
            };

            try
            {
                var str = client.GetStringAsync("/api/identity/").Result;
                Console.WriteLine(str);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }
    }
}
